Rails.application.routes.draw do
  
  get 'password_resets/new'
  get 'welcome/index'
  get 'welcome/somos'
  get 'welcome/cardapio'
  
  get '/login', to: "sessions#new"
  post '/login', to: "sessions#create"
  delete '/logout', to: "sessions#destroy"

  resources :users
  resources :password_resets

  root 'welcome#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
